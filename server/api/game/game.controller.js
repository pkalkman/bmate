'use strict';

var _ = require('lodash');
var Game = require('./game.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * @api {get} /games Get a list of games
 * @apiVersion 0.1.0
 * @apiName GetGames
 * @apiDescription Get all the game.
 * @apiGroup Games
 *
 */
exports.index = function (req, res) {
  Game.find(function (err, games) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(games);
  });
};

/**
 * @api {get} /games/:id Get a single game
 * @apiVersion 0.1.0
 * @apiName GetGame
 * @apiDescription Get only one unique element of game based on its unique id.
 * @apiGroup Games
 *
 * @apiParam {String} id Games unique id.
 *
 */
exports.show = function (req, res) {
  Game.findById(req.params.id, function (err, game) {
    if (err) { return handleError(res, err); }
    if (!game) { return res.status(404).end(); }
    return res.status(200).json(game);
  });
};

/**
 * @api {post} /games Create a new game
 * @apiVersion 0.1.0
 * @apiName CreateGame
 * @apiDescription Creates a new game.
 * @apiGroup Games
 *
 */
exports.create = function (req, res) {
  Game.create(req.body, function (err, game) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(game);
  });
};

/**
 * @api {put} /games/:id Updates an existing game
 * @apiVersion 0.1.0
 * @apiName UpdateGame
 * @apiDescription Update an element of game based on its unique id.
 * @apiGroup Games
 *
 * @apiParam {String} id Games unique id.
 *
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  Game.findById(req.params.id, function (err, game) {
    if (err) { return handleError(res, err); }
    if (!game) { return res.status(404).end(); }
    var updated = _.merge(game, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(game);
    });
  });
};

/**
 * @api {delete} /games/:id Deletes a game
 * @apiVersion 0.1.0
 * @apiName RemoveGame
 * @apiDescription Delete an element of game based on its unique id.
 * @apiGroup Games
 *
 * @apiParam {String} id Games unique id.
 *
 */
exports.destroy = function (req, res) {
  Game.findById(req.params.id, function (err, game) {
    if (err) { return handleError(res, err); }
    if (!game) { return res.status(404).end(); }
    game.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
