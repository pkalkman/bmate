'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GameSchema = new Schema({
  gameDate: {type: Date, default: Date.now},
  turns: Number,
  home: {
    name: String,
    needed: Number,
    caramboles: Number,
    serie: Number,
    points: Number
  },
  away: {
    name: String,
    needed: Number,
    caramboles: Number,
    serie: Number,
    points: Number
  }
});

module.exports = mongoose.model('Game', GameSchema);
