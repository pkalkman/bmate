(function () {
  'use strict';

  angular.module('bmate')
    .controller('SignupCtrl', SignupController);

  function SignupController(Auth, $state) {

    var vm = this;
    vm.user = {};
    vm.signup = signup;

    function signup() {
      Auth.signup(vm.user)
        .then(function () {
          $state.go('games.list');
        });
    }
  }
})();
