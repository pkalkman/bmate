'use strict';

// Setting up route
angular.module('bmate')
  .config(['$stateProvider',

    function ($stateProvider) {

      $stateProvider

        .state('signup', {
          url: '/signup',
          templateUrl: 'views/signup/signup.html',
          controller: 'SignupCtrl as vm'
        });

    }]);
