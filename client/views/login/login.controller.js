(function () {
  'use strict';

  angular.module('bmate')
    .controller('LoginCtrl', LoginController);

  function LoginController(Auth, $state) {

    var vm = this;
    vm.user = {};
    vm.login = login;

    function login() {
      Auth.login(vm.user)
        .then(function () {
          $state.go('games.list');
        });
    }

  }
})();
