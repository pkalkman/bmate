'use strict';

// Setting up route
angular.module('bmate')
  .config(['$stateProvider',

    function ($stateProvider) {

      $stateProvider

        .state('login', {
          url: '/login',
          templateUrl: 'views/login/login.html',
          controller: 'LoginCtrl as vm'
        });

    }]);
