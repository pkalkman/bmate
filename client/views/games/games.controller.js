(function () {
  'use strict';

  angular.module('bmate').controller('GamesCtrl', GamesController);
  angular.module('bmate').controller('SaveGameCtrl', SaveGameController);

  function GamesController($q, GamesService, $state) {

    var vm = this;
    vm.games = [];
    vm.game = {};
    vm.deleteGame = deleteGame;

    activate();

    function activate() {
      var promises = [getGames()];
      return $q.all(promises);
    }

    function getGames() {
      GamesService.getGames().then(function (result) {
        vm.games = result;
      });
    }

    function deleteGame(game){
      GamesService.deleteGame(game).then(function(){
        $state.reload();
      });
    }

  }

  function SaveGameController(GamesService, $state, $stateParams) {

    var vm = this;
    vm.game = {};
    vm.saveGame = saveGame;

    activate();

    function activate() {
      if ($stateParams.gameId) {
        GamesService.getGame($stateParams.gameId).then(function (result) {
          vm.game = result;
        });
      } else{
        vm.game.gameDate = new Date();
      }
    }

    function saveGame() {

      // TODO Why not using upsert?
      if (vm.game._id) {
        GamesService.updateGame(vm.game).then(function () {
          $state.go('games.list');
        });

      } else {
        GamesService.createGame(vm.game).then(function () {
          $state.go('games.list');
        });
      }

    }

  }

})();
