'use strict';

// Setting up route
angular.module('bmate')
  .config(['$stateProvider',

    function ($stateProvider) {

      $stateProvider

        .state('games', {
          abstract: true,
          url: '/games',
          template: '<ui-view/>'
        })

        .state('games.list', {
          url: '',
          templateUrl: 'views/games/views/list-games.html',
          controller: 'GamesCtrl as vm'
        })

        .state('games.new', {
          url: '/new',
          templateUrl: 'views/games/views/edit-game.html',
          controller: 'SaveGameCtrl as vm'
        })

        .state('games.edit', {
          url: '/:gameId/edit',
          templateUrl: 'views/games/views/edit-game.html',
          controller: 'SaveGameCtrl as vm'
        });

    }]);
