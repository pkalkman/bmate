'use strict';

angular.module('bmate')
  .service('GamesService', function ($q, $http, BACKEND_API, $cacheFactory) {

    function updateCache(game, remove) {
      var cache = $cacheFactory.get('$http');
      var gamesCache = cache.get(BACKEND_API + '/games');
      var gamesData = angular.fromJson(gamesCache[1]);

      var index = _.indexOf(gamesData, _.findWhere(gamesData, {_id: game._id}));
      if (remove) {
        gamesData.splice(index, 1);
      } else if (index !== -1) {
        gamesData.splice(index, 1, game);
      } else {
        gamesData.push(game);
      }

      gamesCache[1] = angular.toJson(gamesData);
      cache.put(BACKEND_API + '/games', gamesCache);
    }

    this.getGames = function () {
      var deferred = $q.defer();

      $http.get(BACKEND_API + '/games', {cache: true})
        .then(function (res) {
          deferred.resolve(res.data);
        });
      return deferred.promise;
    };

    this.getGame = function (gameId) {
      var deferred = $q.defer();

      $http.get(BACKEND_API + '/games/' + gameId)
        .then(function (res) {
          deferred.resolve(res.data);
        });
      return deferred.promise;
    };

    this.createGame = function (game) {
      var deferred = $q.defer();

      $http.post(BACKEND_API + '/games', game)
        .then(function (res) {
          updateCache(res.data);
          deferred.resolve(res.data);
        });
      return deferred.promise;
    };

    this.updateGame = function (game) {
      var deferred = $q.defer();

      $http.put(BACKEND_API + '/games/' + game._id, game)
        .then(function (res) {
          updateCache(res.data);
          deferred.resolve(res.data);
        });
      return deferred.promise;
    };

    this.deleteGame = function (game) {
      var deferred = $q.defer();

      $http.delete(BACKEND_API + '/games/' + game._id)
        .then(function () {
          updateCache(game, true);
          deferred.resolve();
        });
      return deferred.promise;
    };

  });
